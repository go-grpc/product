package app

import (
	"bus/product/internal/app/adapter/kafka"
	"bus/product/internal/app/domain/product"
	"bus/product/internal/app/domain/product/db"
	"bus/product/pkg/client/mongodb"
	"context"
	"github.com/joho/godotenv"
	kafka2 "github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/sync/errgroup"
	grpc2 "google.golang.org/grpc"
	"net"
	"os"
)

type App struct {
	r          product.IStorage
	mongo      *mongo.Database
	c          context.Context
	logger     *logrus.Logger
	service    product.IProductService
	processor  kafka.ProductMessageProcessor
	reader     *kafka2.Reader
	listener   net.Listener
	grpcServer *grpc2.Server
}

func New() (*App, error) {
	err := godotenv.Load()
	if err != nil {
		return nil, err
	}
	a := &App{}
	log := logrus.New()
	log.SetFormatter(new(logrus.JSONFormatter))
	a.c = context.Background()
	clientMongo, err := mongodb.NewClient(a.c, "localhost", "27017", "", "", "product", "")
	if err != nil {
		log.Error("failed connect to db")
	}
	a.mongo = clientMongo
	a.logger = log
	a.r = db.NewMongoStorage(a.mongo, "products", a.logger)
	a.service = product.New(a.r, a.logger)
	a.processor = kafka.New(a.logger, a.service)
	a.reader = kafka2.NewReader(kafka2.ReaderConfig{
		Brokers:     []string{"localhost:9092"},
		GroupID:     "my-group",
		GroupTopics: []string{os.Getenv("PRODUCT_UPDATE_EVENT"), os.Getenv("PRODUCT_DELETE_EVENT"), os.Getenv("PRODUCT_CRETE_EVENT")},
	})
	listener, err := net.Listen("tcp", ":5001")
	a.listener = listener
	if err != nil {
		a.logger.Error(err)
	}
	a.grpcServer = grpc2.NewServer()
	return a, nil
}

func (a App) Run() {
	go a.processor.ProcessMessages(a.c, a.reader, 1)
	g, _ := errgroup.WithContext(a.c)
	g.Go(func() error {
		return a.grpcServer.Serve(a.listener)
	})
	err := g.Wait()
	if err != nil {
		a.logger.Error(err)
	}
}
