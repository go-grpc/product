package product

import (
	"bus/product/internal/app/domain/product/model"
	"context"
)

type IStorage interface {
	Create(ctx context.Context, dto model.CreateProductDto) error
	Update(ctx context.Context, id string, dto model.UpdateProductDto) error
	Delete(ctx context.Context, id string) error
	FindOne(ctx context.Context, id string) (model.Product, error)
	Find(ctx context.Context, categories []string) ([]model.Product, error)
	FindByName(ctx context.Context, name string) (model.Product, error)
}
