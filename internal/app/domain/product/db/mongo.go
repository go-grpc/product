package db

import (
	"bus/product/internal/app/domain/product"
	"bus/product/internal/app/domain/product/model"
	"bus/product/pkg/exception"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type db struct {
	collection *mongo.Collection
	logger     *logrus.Logger
}

func (d db) FindByName(ctx context.Context, name string) (p model.Product, err error) {
	d.logger.Debug("product find by name")
	tCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	filter := bson.M{"name": name}
	res := d.collection.FindOne(tCtx, filter)
	if res.Err() != nil {
		return p, fmt.Errorf("failed to find by name")
	}

	if err = res.Decode(&p); err != nil {
		return p, fmt.Errorf("%w", exception.FailedDecodeException)
	}
	return p, nil
}

func (d db) Create(ctx context.Context, dto model.CreateProductDto) error {
	d.logger.Debug("create product")
	tCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	res, err := d.collection.InsertOne(tCtx, dto)
	if err != nil {
		return fmt.Errorf("ошибка при созданий продукта %v", err)
	}
	d.logger.Debug("convert InsertID to ObjectId")
	_, ok := res.InsertedID.(primitive.ObjectID)
	if ok {
		return nil
	}
	d.logger.Trace(dto)
	return fmt.Errorf("ошибка конвертации objectId to Hex %v", mongo.ErrInvalidIndexValue)
}

func (d db) Update(ctx context.Context, id string, dto model.UpdateProductDto) error {
	d.logger.Debug(fmt.Sprintf("update product id of %s", id))
	tCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return fmt.Errorf("failed to convert product Id to ObjectId. ID=%s", id)
	}
	filter := bson.M{"_id": objectID}
	productBytes, err := bson.Marshal(dto)
	if err != nil {
		t := "failed to marshal product."
		d.logger.Error(t)
		return fmt.Errorf("%s error: %v", t, err)
	}
	var updateProductObj bson.M
	err = bson.Unmarshal(productBytes, &updateProductObj)
	if err != nil {
		t := "failed to unmarshal product bytes."
		d.logger.Error(t)
		return fmt.Errorf("%s error: %v", t, err)
	}
	update := bson.M{
		"$set": updateProductObj,
	}
	res, err := d.collection.UpdateOne(tCtx, filter, update)

	if err != nil {
		t := "failed to execute update  product query id product" + id
		d.logger.Error(t)
		return fmt.Errorf("%s error: %v", t, err)
	}
	if res.MatchedCount == 0 {
		t := "not found"
		d.logger.Error(t)
		return fmt.Errorf(t)
	}

	d.logger.Trace(fmt.Sprintf("Matched %d docuemtns and Modified %d documents", res.MatchedCount, res.ModifiedCount))
	return nil
}

func (d db) Delete(ctx context.Context, id string) error {
	d.logger.Debug(fmt.Sprintf("delete one product id of %s", id))
	t := ""
	tCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		t = fmt.Sprintf("failed to convert hex to objectID, hex: %s", id)
		d.logger.Errorf(t)
		return fmt.Errorf(t)
	}

	filter := bson.M{"_id": objectID}
	res, err := d.collection.DeleteOne(tCtx, filter)
	if err != nil {
		return fmt.Errorf("failed delete product")
	}
	if res.DeletedCount == 0 {
		return fmt.Errorf("продукт %w", exception.NotFoundException)
	}
	return nil

}

func (d db) FindOne(ctx context.Context, id string) (p model.Product, err error) {
	t := ""
	d.logger.Debug(fmt.Sprintf("get one product id of %s", id))
	tCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		t = fmt.Sprintf("failed to convert hex to objectID, hex: %s", id)
		d.logger.Errorf(t)
		return p, fmt.Errorf(t)
	}

	filter := bson.M{"_id": oid}

	result := d.collection.FindOne(tCtx, filter)
	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return p, fmt.Errorf("продукт %w", exception.NotFoundException)
		}
	}
	if err = result.Decode(&p); err != nil {
		return p, fmt.Errorf("%w", exception.FailedDecodeException)
	}
	return p, nil

}

func (d db) Find(ctx context.Context, categories []string) (p []model.Product, err error) {
	d.logger.Debug("get products")
	tCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	filter := bson.M{}
	if len(categories) != 0 {
		filter = bson.M{"categories": bson.M{"$in": categories}}
	}

	cursor, err := d.collection.Find(tCtx, filter)

	if err != nil {
		return p, err
	}
	if cursor.Err() != nil {
		return p, err
	}
	if err = cursor.All(tCtx, &p); err != nil {
		return p, fmt.Errorf("%w", exception.FailedReadDocumentsException)
	}

	return p, nil

}

func NewMongoStorage(storage *mongo.Database, collectionName string, logger *logrus.Logger) product.IStorage {
	return db{
		collection: storage.Collection(collectionName),
		logger:     logger,
	}
}
