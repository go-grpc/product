package product

import (
	"bus/product/internal/app/domain/product/model"
	"bus/product/pkg/exception"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
)

type IProductService interface {
	Create(ctx context.Context, event model.CreateProductDto) error
	GetOne(ctx context.Context, id string) (model.Product, error)
	GetAll(ctx context.Context, categories []string) ([]model.Product, error)
	Update(ctx context.Context, id string, dto model.UpdateProductDto) error
	Delete(ctx context.Context, id string) error
	GetByName(ctx context.Context, name string) (model.Product, error)
}

type service struct {
	d      IStorage
	logger *logrus.Logger
}

func (s service) Create(ctx context.Context, dto model.CreateProductDto) error {
	fmt.Println(dto.Price)
	s.logger.Debug("create product")
	candidate, err := s.GetByName(ctx, dto.Name)
	if err != nil {
		return err
	}
	if candidate.Id != "" {
		return fmt.Errorf("такой продукт %w", exception.ExistException)
	}
	err = s.d.Create(ctx, dto)
	if err != nil {
		return err
	}
	return nil
}

func (s service) GetOne(ctx context.Context, id string) (model.Product, error) {
	s.logger.Debug("get one product")
	product, err := s.d.FindOne(ctx, id)
	if err != err {
		return product, err
	}
	return product, nil
}

func (s service) GetAll(ctx context.Context, categories []string) ([]model.Product, error) {
	s.logger.Debug("get all products")
	products, err := s.d.Find(ctx, categories)
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (s service) Update(ctx context.Context, id string, dto model.UpdateProductDto) error {
	s.logger.Debug("update product")
	err := s.d.Update(ctx, id, dto)
	if err != nil {
		return err
	}
	return nil
}

func (s service) Delete(ctx context.Context, id string) error {
	s.logger.Debug("delete product")
	err := s.d.Delete(ctx, id)
	if err != nil {
		return err
	}
	return nil
}

func (s service) GetByName(ctx context.Context, name string) (model.Product, error) {
	s.logger.Debug("get product by name")
	product, err := s.d.FindByName(ctx, name)
	if err != nil {
		return product, err
	}
	return product, err
}

func New(d IStorage, logger *logrus.Logger) IProductService {
	return service{d, logger}
}
