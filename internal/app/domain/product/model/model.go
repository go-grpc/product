package model

type Product struct {
	Id          string   `json:"id" bson:"_id"`
	Price       int      `json:"price"`
	Name        string   `json:"name" bson:"name"`
	Description string   `json:"description" bson:"description"`
	Image       string   `json:"image" bson:"image"`
	Categories  []string `json:"categories" bson:"categories"`
}

type UpdateProductDto struct {
	Id string `json:"id"`
}

type CreateProductDto struct {
	Price       int      `json:"price"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Image       string   `json:"image"`
	Categories  []string `json:"categories"`
}
type UpdateProductEvent struct {
	Id string `json:"id"`
	CreateProductDto
}

type DeleteProductEvent struct {
	Id string `json:"id"`
}
