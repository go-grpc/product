package kafka

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (p ProductMessageProcessor) deleteProductProcessor(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	p.logger.Debug("start working delete processor")
	defer p.logger.Debug("end process delete product")
	var msg string
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		p.logger.Error("error in unmarshal")
		return
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		fmt.Println(p.service)
		return p.service.Delete(ctx, msg)
	})
	err := g.Wait()
	if err != nil {
		p.logger.Error("error in delete product")
		return
	}
	p.logger.Debug("success process deleting product")
}
