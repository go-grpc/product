package kafka

import (
	"bus/product/internal/app/domain/product/model"
	"context"
	"encoding/json"
	"fmt"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (p ProductMessageProcessor) createProductProcessor(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	p.logger.Debug("start process creating order")
	defer p.logger.Debug("end process creating order")
	var msg model.CreateProductDto
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		p.logger.Error("error in unmarshal")
		return
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		fmt.Println(p.service)
		return p.service.Create(ctx, msg)
	})
	err := g.Wait()
	if err != nil {
		p.logger.Error("error in creating order")
		return
	}
	p.logger.Debug("success process creating order")
}
