package kafka

import (
	"bus/product/internal/app/domain/product/model"
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
	"os"
)

type service interface {
	Create(ctx context.Context, event model.CreateProductDto) error
	Update(ctx context.Context, id string, dto model.UpdateProductDto) error
	Delete(ctx context.Context, id string) error
}

type ProductMessageProcessor struct {
	logger  *logrus.Logger
	service service
}

func New(logger *logrus.Logger, service service) ProductMessageProcessor {
	return ProductMessageProcessor{logger, service}
}

func (p ProductMessageProcessor) ProcessMessages(ctx context.Context, r *kafka.Reader, workerID int) {
	for {
		select {
		case <-ctx.Done():
			return
		default:

		}

		m, err := r.FetchMessage(ctx)
		fmt.Println(err)
		if err != nil {
			p.logger.Error(err, workerID)
			continue
		}
		switch m.Topic {
		case os.Getenv("ORDER_CRETE_EVENT"):
			p.createProductProcessor(ctx, r, m)
		case os.Getenv("ORDER_UPDATE_EVENT"):
			p.updateProductProcessor(ctx, r, m)
		case os.Getenv("ORDER_DELETE_EVENT"):
			p.deleteProductProcessor(ctx, r, m)
		}

	}
}
