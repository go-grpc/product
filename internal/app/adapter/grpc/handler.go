package grpc

import (
	"bus/product/internal/app/adapter/grpc/proto"
	"bus/product/internal/app/domain/product/model"
	"bus/product/pkg/exception"
	"context"
	"errors"
	"google.golang.org/grpc"
)

type service interface {
	GetOne(ctx context.Context, id string) (model.Product, error)
	GetAll(ctx context.Context, categories []string) ([]model.Product, error)
	GetByName(ctx context.Context, name string) (model.Product, error)
}
type server struct {
	s service
}

func (s server) GetProducts(ctx context.Context, request *proto.GetProductsRequest) (*proto.GetProductResponse, error) {
	products, err := s.s.GetAll(ctx, request.GetCategories())
	response := &proto.Response{
		Status: 200,
		Error:  "",
	}

	if err != nil {
		return &proto.GetProductResponse{
			Products: nil,
			Response: handler(err, response),
		}, nil
	}
	var sliceProducts []*proto.ProductItem
	for _, product := range products {
		sliceProducts = append(sliceProducts, convertJSONToGrpc(product))
	}
	return &proto.GetProductResponse{Products: sliceProducts, Response: response}, nil
}

func (s server) GetOrderOneById(ctx context.Context, request *proto.GetProductByIdRequest) (*proto.GetProductOneResponse, error) {
	product, err := s.s.GetOne(ctx, request.Id)
	response := &proto.Response{
		Status: 200,
		Error:  "",
	}
	if err != nil {
		return &proto.GetProductOneResponse{
			Product:  nil,
			Response: handler(err, response),
		}, nil
	}
	return &proto.GetProductOneResponse{
		Product:  convertJSONToGrpc(product),
		Response: response,
	}, nil
}

func (s server) GetOrderOneByName(ctx context.Context, request *proto.GetProductByNameRequest) (*proto.GetProductOneResponse, error) {
	product, err := s.s.GetByName(ctx, request.Name)
	response := &proto.Response{
		Status: 200,
		Error:  "",
	}
	if err != nil {
		return &proto.GetProductOneResponse{
			Product:  nil,
			Response: handler(err, response),
		}, nil
	}
	return &proto.GetProductOneResponse{
		Product:  convertJSONToGrpc(product),
		Response: response,
	}, nil
}

func New(gServer *grpc.Server, s service) {
	server := server{s}
	proto.RegisterOrderServiceServer(gServer, server)
}

func handler(err error, response *proto.Response) *proto.Response {
	if errors.Is(err, exception.NotFoundException) {
		response.Status = 404
		response.Error = err.Error()
		return response

	}
	if errors.Is(err, exception.FailedDecodeException) {
		response.Status = 500
		response.Error = err.Error()
		return response
	}
	if errors.Is(err, exception.FailedReadDocumentsException) {
		response.Status = 422
		response.Error = err.Error()
		return response
	}
	if errors.Is(err, exception.ExistException) {
		response.Status = 400
		response.Error = err.Error()
		return response
	}
	response.Status = 500
	response.Error = err.Error()
	return response
}

func convertJSONToGrpc(product model.Product) *proto.ProductItem {
	return &proto.ProductItem{
		Id:          product.Id,
		Price:       uint32(product.Price),
		Name:        product.Name,
		Description: product.Description,
		Image:       product.Image,
		Categories:  product.Categories,
	}
}
