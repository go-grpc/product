package exception

import "errors"

type ServerError struct {
	err error
}

func (s ServerError) Error() string {
	return s.err.Error()
}

func New(msg string) error {
	return &ServerError{err: errors.New(msg)}
}

var NotFoundException = New("не найден")

var ExistException = New("уже существует")

var FailedReadDocumentsException = New("failed to read all documents")

var FailedDecodeException = New("failed decode")
