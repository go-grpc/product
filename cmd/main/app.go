package main

import "bus/product/internal/pkg/app"

func main() {
	a ,err := app.New()
	if err !=nil{
		panic(err)
	}
	a.Run()
}

